import Phaser from "phaser";
import config from "visual-config-exposer";
var fruits = [
  "apple",
  "banana",
  "cherry",
  "dragon-fruit",
  "orange",
  "pineapple",
  "pumpkin",
  "strawberry",
];

var random2;

class PreGame extends Phaser.Scene {
  constructor() {
    super("bootGame");
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  preload() {
    random2 = Math.floor(Math.random() * (fruits.length - 1) + 1);
    document.body.style.background =
      "url(" + config.preGameScreen.GameBackgroundImage + ")";
    this.load.image("background", config.preGameScreen.BackgroundImage);
    this.load.audio("backmusic", config.settings.Music);
    this.load.audio("splash", "./public/assets/sounds/splatter.mp3");
    this.load.audio("missed", "./public/assets/sounds/missed.mp3");
    this.load.audio("over", "./public/assets/sounds/over.mp3");
    this.load.audio("start", "./public/assets/sounds/start.mp3");
    this.load.audio("explode", "./public/assets/sounds/boom.mp3");

    this.load.image("bomb", "./images/bomb.png");

    this.load.image("logo", config.preGameScreen.GameLogo);

    this.load.html("form", "../assets/form.html");
  }

  create() {
    // ALL THIS CODE IS FOR RESPONSIVENESS AND RESIZING
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;

    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.FIT,
        this.parent
      );
    }

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on("resize", this.resize, this);

    //
    // ADD GAME CODE BELOW
    //..
    //..
    let bg = this.add.sprite(0, 0, "background");
    bg.setOrigin(0, 0).setDisplaySize(740, 900);

    const logo = this.add.image(375, 300, "logo").setScale(2);

    const title = this.add.text(
      this.GAME_WIDTH / 3,
      this.GAME_HEIGHT / 10,
      "Fruit Ninja",
      {
        fontSize: "45px",
        fontStyle: "bold",
        fill: config.settings.textColor,
      }
    );
    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.15,
      this.GAME_HEIGHT / 2,
      "Play",
      {
        fontSize: "32px",
        fill: config.settings.textColor,
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 2 + 100,
      "LeaderBoards",
      {
        fontSize: "32px",
        fill: config.settings.textColor,
      }
    );

    // already coded
    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();

    playBtn.on("pointerdown", () => {
      //    this.scene.start('playGame'); // **** This is the line used for switching scenes
    });

    playBtn.on("pointerdown", () => {
      // console.log('redirect to play');
      this.scene.start("playthis");
    });

    leaderBoardBtn.on("pointerdown", () => {
      //hoverImage.setVisible(true);
      // console.log('Play Out');
      this.scene.start("leaderboard");
    });

  /*  leaderBoardBtn.on("pointerout", () => {
      hoverImage.setVisible(false);
      // console.log('Play Out');
    });*/

    this.scale.on("resize", this.resize, this);
  }

  // BELOW FUNCTIONS ARE USED FOR RESIZING

  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    // this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;
    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;
    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }

  getZoom() {
    return this.cameras.main.zoom;
  }
}

export default PreGame;
