import Phaser, { Data, Tilemaps } from "phaser";
import config from "visual-config-exposer";

var fruits = [
  
  "banana",
  "apple",
  "peach",
  "watermelon",
  "strawberry",
];
var fruit,
  bomb,
  splatter,
  missed,
  over,
  start,
  boom,
  explode,
  particles,
  tipLabel,
  fruitcut,
  fruitcut2,
  scoreLabel,
  pointer3,
  chromeLabel,
  Highscore = 0,
  timer,
  fruitpoint,
  fontSize,
  fruitSize,
  score2,
  explosion,
  timerCount = 10,
  contactPoint,
  randomh,
  objects,
  pointer,
  pointer2,
  splashs,
  score = 0,
  line,
  play,
  points = [];
var score3 = 0;
var random;
var randomNum = Math.floor(Math.random() * (fruits.length - 1) + 1);
var fireRate = 1400;
var nextFire = 0;
var n = 2;
localStorage.setItem("score", 0);
localStorage.setItem("Highscore", 0);
var store = parseInt(localStorage.getItem("Highscore"));
class Game1 extends Phaser.Scene {
  constructor() {
    super("playthis");
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 900;
  }

  preload() {
    this.load.image("fruit0", `./public/assets/images/fruits/${fruits[0]}.png`);
    this.load.image("fruit1", `./public/assets/images/fruits/${fruits[1]}.png`);
    this.load.image("fruit2", `./public/assets/images/fruits/${fruits[2]}.png`);
    this.load.image("fruit3", `./public/assets/images/fruits/${fruits[3]}.png`);
    this.load.image("fruit4", `./public/assets/images/fruits/${fruits[4]}.png`);
    this.load.image("fruit5", `./public/assets/images/fruits/${fruits[5]}.png`);
    this.load.image("fruit6", `./public/assets/images/fruits/${fruits[6]}.png`);
    this.load.image("blade", "./public/assets/images/cut.png");
    this.load.image("explosion", "./public/assets/images/explosion.png");
    this.load.image("boom", "./public/assets/images/boom.png");
    this.load.image("splashs", "./public/assets/images/splash.png");
    this.load.image("apple-1", "./public/assets/images/fruits/apple-1.png")
    this.load.image("apple-2", "./public/assets/images/fruits/apple-2.png")
    this.load.image("strawberry-1", "./public/assets/images/fruits/strawberry-1.png")
    this.load.image("strawberry-2", "./public/assets/images/fruits/strawberry-2.png")
    this.load.image("watermelon-1", "./public/assets/images/fruits/watermelon-1.png")
    this.load.image("watermelon-2", "./public/assets/images/fruits/watermelon-2.png")
    this.load.image("peach-1", "./public/assets/images/fruits/peach-1.png")
    this.load.image("peach-2", "./public/assets/images/fruits/peach-2.png")
    this.load.image("banana-1", "./public/assets/images/fruits/banana-1.png")
    this.load.image("banana-2", "./public/assets/images/fruits/banana-2.png")
  }

  create(event) {
    timer = 10;
    const width = this.scale.gameSize.width;
    const height = this.scale.gameSize.height;
    this.parent = new Phaser.Structs.Size(width, height);
    if (window.innerWidth < 800) {
      this.sizer = new Phaser.Structs.Size(
        (this.GAME_WIDTH = 740), //600
        (this.GAME_HEIGHT = 900), //1000
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    } else if (window.innerWidth >= 800) {
      this.sizer = new Phaser.Structs.Size(
        this.GAME_WIDTH,
        this.GAME_HEIGHT,
        Phaser.Structs.Size.ENVELOP,
        this.parent
      );
    }
    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);
    this.updateCamera();
    this.scale.on("resize", this.resize, this);

    /*this.physics.world.setBounds(0, 0, this.GAME_WIDTH, this.GAME_HEIGHT);*/

    let bg = this.add.sprite(0, 0, "background");
    bg.setOrigin(0, 0).setDisplaySize(740, 1000);
    splatter = this.sound.add("splash");
    missed = this.sound.add("missed");
    over = this.sound.add("over");
    start = this.sound.add("start");
    start.play();
    explode = this.sound.add("explode");
    scoreLabel = this.add.text(500, 10, `Score: ${score}`, {
      fontSize: "30px",
    });

   splashs = this.add.image(0, 0, "splashs").setDisplaySize(100, 100);
    splashs.visible = false;

    /*  Highscore = this.add.text(50, 10, `Highscore: ${Highscore}`, {
      fontSize: "30px",
    });*/

    //pointer3 = this.add.line(200, 200, 0, 0, -50, 100, 0xffffff);

    /*pointer2.visible = false;*/

    pointer2 = this.add.image(200, 200, "blade").setDisplaySize(50, 100);
    pointer2.visible = false;

    timer = this.add.text(500, 50, `Timer: ${timerCount}`, {
      fontSize: "30px",
    });

    setInterval(() => {
      if (timerCount == 0) {
        this.scene.start("endGame");
        over.play();
      }

      timerCount--;
    }, 1000);

    setInterval(() => {
      for (var n = 1; n < 2; n++) {
        var num = Math.floor(Math.random() * 15 + 1);
        bomb = this.physics.add
          .image(50 * num, -50 * num, "bomb")
          .setDisplaySize(80, 80);
        boom = this.physics.add
          .image(50 * num, -50 * num, "boom")
          .setDisplaySize(80, 80);
        boom.setVisible(false);
      }
    }, 2000);

    setInterval(() => {
      for (var n = 1; n < 2; n++) {
        var num = Math.floor(Math.random() * (fruits.length - 1) + 0);

        fruit = this.physics.add
          .sprite(50 * (num + 2), -1 * num, `fruit${num}`)
          .setDisplaySize(80, 80);
        var name = fruits[num]
        console.log(name)
         fruitcut = this.physics.add.sprite(50*(num+2), -1*num, `${name}-1`)
         fruitcut.visible=false
         fruitcut2 = this.physics.add.sprite(50*(num+3), 3*num, `${name}-2`)
         fruitcut2.visible = false

        particles = this.physics.add
          .sprite(50 * (num + 2), -1 * num, "explosion")
          .setDisplaySize(100, 100)
          .setVisible(false);
      }
    }, 2000);
  }

  update(event) {
    
    score2 = localStorage.getItem("score");

    timer.text = `Timer : ${timerCount}`;

    pointer = this.input.on("pointerdown", function (pointer) {
      if (
        fruit.x + 100 >= pointer.worldX &&
        fruit.x - 100 <= pointer.worldX &&
        fruit.y + 100 >= pointer.worldY &&
        fruit.y - 100 <= pointer.worldY
      ) {
        splashs.x = pointer.worldX;
        splashs.y = pointer.worldY;
        splashs.visible=true
       
        fruitcut.visible=true
        fruitcut2.visible=true
        score3 = parseInt(score2) + 5;
        localStorage.setItem("score", score3);
        scoreLabel.text = `Score: ${score3}`;
        fruit.destroy();
        splatter.play();
       // particles.visible = true;
      } else {
        missed.play();
      }
      if (
        bomb.x + 100 >= pointer.worldX &&
        bomb.x - 100 <= pointer.worldX &&
        bomb.y + 100 >= pointer.worldY &&
        bomb.y - 100 <= pointer.worldY
      ) {
        bomb.destroy();
        explode.play();
        boom.visible = true;
        score3 = parseInt(score2) - 5;
        localStorage.setItem("score", score3);
        scoreLabel.text = `Score: ${score3}`;
      }
      if (score3 > store) {
        localStorage.setItem("Highscore", score3);
      }

      pointer2.x = pointer.worldX;
      pointer2.y = pointer.worldY;
      pointer2.visible = true;
      setTimeout(() => {
        pointer2.visible = false;
      }, 300);
      setTimeout(() => {
        splashs.visible = false;
      }, 1000);
    });

    random = Math.floor(Math.random() * fruits.length + 1);
  }

  storeScore() {
    localStorage.setItem("score", 0);
  }
  resize(gameSize) {
    const width = gameSize.width;
    const height = gameSize.height;

    this.parent.setSize(width, height);
    this.sizer.setSize(width, height);

    this.updateCamera();
  }

  updateCamera() {
    const camera = this.cameras.main;

    const x = Math.ceil((this.parent.width - this.sizer.width) * 0.5);
    const y = 0;
    const scaleX = this.sizer.width / this.GAME_WIDTH;
    const scaleY = this.sizer.height / this.GAME_HEIGHT;

    camera.setViewport(x, y, this.sizer.width, this.sizer.height);
    camera.setZoom(Math.max(scaleX, scaleY));
    camera.centerOn(this.GAME_WIDTH / 2, this.GAME_HEIGHT / 2);
  }
}

export default Game1;
